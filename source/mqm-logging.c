/*-
 * SPDX-License-Identifier: Apache-2.0
 *
 * Copyright 2021 Huawei
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

/*
 * @author Alin Popa <alin.popa@fxdata.ro>
 * @file mqm-logging.c
 */

#include "mqm-logging.h"
#include "mqm-types.h"

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <syslog.h>

static void mqm_logging_handler(const gchar *log_domain,
                                GLogLevelFlags log_level,
                                const gchar *message,
                                gpointer user_data);

static int priority_to_syslog(int priority)
{
    switch (priority) {
    case G_LOG_FLAG_RECURSION:
    case G_LOG_FLAG_FATAL:
        return LOG_CRIT;
    case G_LOG_LEVEL_ERROR:
    case G_LOG_LEVEL_CRITICAL:
        return LOG_ERR;
    case G_LOG_LEVEL_WARNING:
        return LOG_WARNING;
    case G_LOG_LEVEL_MESSAGE:
    case G_LOG_LEVEL_INFO:
        return LOG_INFO;
    case G_LOG_LEVEL_DEBUG:
    default:
        return LOG_DEBUG;
    }
}

void mqm_logging_open(const gchar *app_name,
                      const gchar *app_desc,
                      const gchar *ctx_name,
                      const gchar *ctx_desc)
{
    MQM_UNUSED(app_name);
    MQM_UNUSED(app_desc);
    MQM_UNUSED(ctx_name);
    MQM_UNUSED(ctx_desc);

    g_log_set_default_handler(mqm_logging_handler, NULL);
}

static void mqm_logging_handler(const gchar *log_domain,
                                GLogLevelFlags log_level,
                                const gchar *message,
                                gpointer user_data)
{
    MQM_UNUSED(log_domain);
    MQM_UNUSED(user_data);

    syslog(priority_to_syslog(log_level), "%s", message);
}

void mqm_logging_close(void) { }
